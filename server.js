'use strict';

const express = require('express');
const path = require('path');
const bunyan = require('bunyan');
const Elasticsearch = require('bunyan-elasticsearch');
const { v4: uuidv4 } = require('uuid');

const esStream = new Elasticsearch({
  indexPattern: '[labtest-]YYYY.MM.DD',
  type: 'logs',
  host: 'localhost:9200'
});
esStream.on('error', function (err) {
  console.log('Elasticsearch Stream Error:', err.stack);
});

const log = bunyan.createLogger({
    name: 'myapp',
    streams: [
        { stream: esStream },
        { stream: process.stdout }
    ],
    serializers: bunyan.stdSerializers
});

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/capturar/:animal', (req, res) => {

    const optLogs = {
        "RequestID" : uuidv4()
    };

    log.info(optLogs, "Iniciando captura de animal");

    const animal = req.params.animal;

    log.info(optLogs, "Foi caturado um %s", animal);

    res.sendFile(path.join(__dirname+'/index.html'));
});

app.listen(PORT, HOST);
log.info(`Running on http://${HOST}:${PORT}`);